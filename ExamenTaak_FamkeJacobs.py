import requests
import yaml
import json
import re
 
api_base_url = 'https://api.domainsdb.info/v1/domains/search?domain=syntra.be'
 
response = requests.get(f"{api_base_url}")
 
response_data = json.loads(response.text)
#json.loads neemt - " - over als een - ' -. Hierbij moet je rekening houden in je regexen.
 
"""VRAAG 2: Bovenstaan is de manier op een API aantespreken in python. Wil je deze volledig printen
gebruik dan de comment print(repr(response_data)."""
 
 
output =(repr(response_data))
 
regex_datum = r"'create_date\':\s\'(?P<jaar>\d{4})-(?P<maand>\d{2})-(?P<dag>\d{2})"
regex_providor = r"ns\d\.(?P<Providor>\w+)"
regex_land = r"country\'\:\s\'(?P<land>\w+)\'"
regex_IP = r"(?P<IP>(?:[0-9]{1,3}\.){3}[0-9]{1,3})"
regex_list = [regex_datum, regex_providor, regex_land, regex_IP]
 
"""VRAAG 3: Door bovenstaande regexen toetepassen krijg je de gewenste output die wordt gevraagd in opdracht drie. Ook vind je in deze sectie de regexen terug die
nodig zijn voor de output die gevraagd wordt in vraag 4."""
 
result = {}
 
for regex in regex_list:
    for match in re.finditer(regex, output,  flags=re.MULTILINE):
        result = result | match.groupdict()
 
print(yaml.dump(result))

"""VRAAG4: door bovenstaand toetepassen krijg je in YAML output."""